using System;
using Newtonsoft.Json;

namespace speedtest_net_auto_clipboard
{

    class Program
    {
        static void Main(string[] args)
        {
            string serverId;
            string serverArg;

            try
            {
                serverId = args[0];
                serverArg = $"-s {serverId}";
            }
            catch (IndexOutOfRangeException)
            {
                serverArg = "";
            }

            Console.WriteLine("\nUsage: speedtest-image <server ID>\n");
            Console.WriteLine("Running internet speedtest...\n");


            try
            {

                System.Diagnostics.Process p1 = new System.Diagnostics.Process();
                p1.StartInfo.FileName = "cmd.exe";
                p1.StartInfo.Arguments = @"/C speedtest -p no -f json --accept-gdpr " + serverArg;
                p1.StartInfo.UseShellExecute = false;
                p1.StartInfo.RedirectStandardOutput = true;
                p1.Start();

                string cmdOutJson = p1.StandardOutput.ReadToEnd();

                p1.WaitForExit();


                // Convert JSON to a C# object
                var cmdOutObj = JsonConvert.DeserializeObject<dynamic>(cmdOutJson);
                string resultUrl = cmdOutObj.result.url;
                string resultPngUrl = resultUrl + ".png";


                System.Diagnostics.Process p2 = new System.Diagnostics.Process();
                p2.StartInfo.FileName = "cmd.exe";
                p2.StartInfo.Arguments = $@"/C echo|set/p={resultPngUrl}|clip";
                p2.StartInfo.UseShellExecute = false;
                p2.StartInfo.RedirectStandardOutput = true;
                p2.Start();
                p2.WaitForExit();
            }
            catch (System.Exception e)
            {
                Console.WriteLine("\n\n    !!! An error occurred: " + e.Message);
                Console.WriteLine("    !!! Probably either 'speedtest.exe' or 'Newtonsoft.Json.dll' aren't present or are outdated.");
            }

    
            Console.WriteLine("\n\n");
            Console.WriteLine("If no errors occurred, the speedtest was complete and the result's image URL should be in your clipboard, ready to paste!\n");
            System.Diagnostics.Process p3 = new System.Diagnostics.Process();
            p3.StartInfo.FileName = "cmd.exe";
            p3.StartInfo.Arguments = $@"/C pause";
            p3.StartInfo.UseShellExecute = false;
            //p3.StartInfo.RedirectStandardOutput = true;
            p3.Start();
            p3.WaitForExit();
        }
    }
}
