# speedtest-image

**THIS IS A WINDOWS ONLY PROGRAM**

A program that runs an internet speedtest via speedtest.net and copies the result's image URL to the clipboard.

Obtain pre-compiled binaries [here](https://gitlab.com/rafaeru/speedtest-net-auto-clipboard/-/releases).

## Dependencies for running

* **`speedtest`** (comes in the zip file with this program)  
You can also install it via [Chocolatey](https://chocolatey.org/packages/speedtest), Scoop or download it from its [official website](https://www.speedtest.net/apps/cli). Just make sure the file is either in the same folder as this program or the folder which contains the file is in your `PATH` environment variable. You don't have to worry about this if you install it via Chocolatey or Scoop.

* **[.NET Framework 4.8](https://dotnet.microsoft.com/download/dotnet-framework)** (comes pre-installed in recent versions of Windows 10)
